let landingscene = new THREE.Scene(); 
let landingcamera = new THREE.PerspectiveCamera(60, window.innerWidth/window.innerHeight,1,1000);
let landingLoader = new THREE.TextureLoader();
let cloudParticles =[];
let composer;

let mouseX = 0, mouseY = 0;
let windowHalfX = window.innerWidth / 2;
let windowHalfY = window.innerHeight / 2;


function landinginit(){
    landingscene;
    landingcamera;
    landingcamera.position.z = 1;
    landingcamera.rotation.x = 1.2;
    landingcamera.rotation.y = -0.15;
    landingcamera.rotation.z = 0.3;

    let landingAmbientLight = new THREE.AmbientLight(0x555555);
    landingscene.add(landingAmbientLight);

    let landingDirectionalLight= new THREE.DirectionalLight(0xff8c19);
    landingDirectionalLight.position.set(0,0,1);
    landingscene.add(landingDirectionalLight);

    let orangeLight = new THREE.PointLight(0xcc6600, 50, 450, 1.7);
    orangeLight.position.set(500,200,100);
    landingscene.add(orangeLight);

    let redLight = new THREE.PointLight(0xd8547e, 50, 450, 1.7);
    redLight.position.set(300,200,100);
    landingscene.add(redLight);

    let blueLight = new THREE.PointLight(0x1b34b3, 50, 450, 1.7);
    blueLight.position.set(600,100,200);
    landingscene.add(blueLight);

    landingrenderer = new THREE.WebGLRenderer({alpha:true});
    landingrenderer.setSize(window.innerWidth, window.innerHeight);
    landingFog = new THREE.FogExp2(0xffffff, 0.0001);
    landingrenderer.setClearColor(0x000000, 0);
    document.getElementById("landingcanvas").appendChild(landingrenderer.domElement);

    landingLoader.load("assets/textures/cloud.png", function(texture){
    landingCloudGeometry = new THREE.PlaneBufferGeometry(800,800);
    landingCloudMaterial = new THREE.MeshLambertMaterial({
        map:texture,
        transparent: true
    });

    for(let c = 0; c<100; c++){
        let landingCloud = new THREE.Mesh(landingCloudGeometry, landingCloudMaterial);
        landingCloud.position.set(
            Math.random()*1500 - 500, 500, Math.random()*300-300
        );

    landingCloud.rotation.x = 1.2;
    landingCloud.rotation.y = -0.15
    landingCloud.rotation.z = Math.random() * Math.PI;
    landingCloud.opacity = 0.55;
    cloudParticles.push(landingCloud);
    landingscene.add(landingCloud);
    }
});


}

$(document).ready(function () {
    $("#welcome").fadeIn(5000);
  });

$("#enter").click(function() {
    $("#enter").fadeOut(1000);
    $(".menuitem").fadeIn(3000);
    $("#name").fadeIn(3000);
});

$("#projects").click(function() {
    $(".menuitem").fadeOut(1000);
    $("#carouselcontainer").fadeIn(3000);
    $(".next").fadeIn(3000);
    $(".prev").fadeIn(3000);
    $(".bottomnav").fadeIn(3000);
    $(".btmbtn").removeClass("active");
    $(".projectsbtn").addClass("active");
});

$(".projectsbtn").click(function() {
    $("#carouselcontainer").fadeIn(3000);
    $(".next").fadeIn(3000);
    $(".prev").fadeIn(3000);
    $("#contactpage").fadeOut(1000);
    $(".aboutcontainer").fadeOut(1000);
    $(".btmbtn").removeClass("active");
    $(".projectsbtn").addClass("active");
});

$("#about").click(function() {
    $(".menuitem").fadeOut(1000);
    $(".aboutcontainer").fadeIn(3000);
    $(".bottomnav").fadeIn(3000);
    $(".btmbtn").removeClass("active");
    $(".aboutbtn").addClass("active");
});

$(".aboutbtn").click(function() {
    $(".aboutcontainer").fadeIn(3000);
    $("#carouselcontainer").fadeOut(1000);
    $(".next").fadeOut(1000);
    $(".prev").fadeOut(1000);
    $("#contactpage").fadeOut(1000);
    $(".btmbtn").removeClass("active");
    $(".aboutbtn").addClass("active");
});

$("#contact").click(function() {
    $(".menuitem").fadeOut(1000);
    $("#contactpage").fadeIn(3000);
    $(".bottomnav").fadeIn(3000);
    $(".btmbtn").removeClass("active");
    $(".contactbtn").addClass("active");
});

$(".contactbtn").click(function() {
    $("#contactpage").fadeIn(3000);
    $("#carouselcontainer").fadeOut(1000);
    $(".aboutcontainer").fadeOut(1000);
    $(".next").fadeOut(1000);
    $(".prev").fadeOut(1000);
    $(".btmbtn").removeClass("active");
    $(".contactbtn").addClass("active");
});

$("#homelink").click(function() {
    $(".bottomnav").fadeOut(1000);
    $(".btmbtn").removeClass("active");
    $(".menuitem").fadeIn(3000);
    $("#carouselcontainer").fadeOut(1000);
    $("#contactpage").fadeOut(1000);
    $(".aboutcontainer").fadeOut(1000);
    $(".next").fadeOut(1000);
    $(".prev").fadeOut(1000);
});

$("#sendbtn").click(function(){
    alert("Thank you for your message! Please expect my reply within the next 24 hours.");
});


var carousel = $(".carousel"),
    currdeg  = 0;

$(".next").on("click", { d: "n" }, rotate);
$(".prev").on("click", { d: "p" }, rotate);

function rotate(e){
  if(e.data.d=="n"){
    currdeg = currdeg - 72;
  }
  if(e.data.d=="p"){
    currdeg = currdeg + 72;
  }
  carousel.css({
    "-webkit-transform": "rotateY("+currdeg+"deg)",
    "-moz-transform": "rotateY("+currdeg+"deg)",
    "-o-transform": "rotateY("+currdeg+"deg)",
    "transform": "rotateY("+currdeg+"deg)"
  });

}



landingLoader.load("assets/images/moon.jpg", function(texture){

    const textureEffect = new POSTPROCESSING.TextureEffect({
        blendFunction: POSTPROCESSING.BlendFunction.COLOR_DODGE,
        texture: texture
    });
    textureEffect.blendMode.opacity.value = 0.2;
    const bloomEffect = new POSTPROCESSING.BloomEffect({
        blendFunction: POSTPROCESSING.BlendFunction.COLOR_DODGE,
        kernelSize: POSTPROCESSING.KernelSize.Small,
        useLuminanceFilter: true,
        luminanceTreshold: 0.3,
        luminanceSmoothing: 0.75
    });
    bloomEffect.blendMode.opacity.value = 1.5;

    let effectPass = new POSTPROCESSING.EffectPass(
        landingcamera,
        bloomEffect,
        textureEffect
    );
    effectPass.renderToScreen = true;

    landingcomposer = new POSTPROCESSING.EffectComposer(landingrenderer); 
    landingcomposer.addPass(new POSTPROCESSING.RenderPass(landingscene, landingcamera));
    landingcomposer.addPass(effectPass);
    animate();
});

function animate(){
    cloudParticles.forEach(p => {
        p.rotation.z -= 0.002
    });
    landingcomposer.render(0.1);
    requestAnimationFrame(animate);
}

function onDocumentMouseMove( event ) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
}

function onDocumentTouchStart( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
        }
}

function onDocumentTouchMove( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        mouseY = event.touches[ 0 ].pageY - windowHalfY;
                }
            }


function onWindowResize(event) {

landingcamera.aspect = window.innerWidth / window.innerHeight;
landingcamera.updateProjectionMatrix();
landingrenderer.setSize( window.innerWidth, window.innerHeight );

}

document.addEventListener( 'mousemove', onDocumentMouseMove, false );
document.addEventListener( 'touchstart', onDocumentTouchStart, false );
document.addEventListener( 'touchmove', onDocumentTouchMove, false );

window.addEventListener( 'resize', onWindowResize, false );

landinginit();